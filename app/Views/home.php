<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Authentication App (Fakhri)</title>

    <!-- Custom fonts for this template-->
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">


    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <style>
        html,
        body {
            height: 100%;
        }

        .container {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
        }

        .paragraph {
            color: white;
        }

        .paragraph .p-caption {
            font-size: 14px;
            font-weight: 500;
        }

        .paragraph .p-title {
            font-size: 16px;
            font-weight: 300;
        }
    </style>
</head>

<body class="bg-primary">

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top ">
        <nav class=" navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="col-5">
                    <a class=" navbar-brand" href="<?= base_url(); ?>"><strong>CORALIS STUDIO</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="col d-flex justify-content-end">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active" href="<?= base_url(); ?>">Home</a>
                        <a class="nav-item nav-link " href="<?= base_url(); ?>login">Login</a>
                        <a class="nav-item nav-link" href="<?= base_url(); ?>register">Register</a>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- End Header -->

    <div class="container">
        <div class="row text-center paragraph" style="color:white">
            <!-- Your content goes here -->
            <h1>Selamat Datang di Website Login Coralis Studio</h1>
            <p class="p-caption">Website ini merupakan sebuah project kecil untuk test melamar kerja pada Coralis Studio
                sebagai Junior
                Web
                Developer</p>
            <p class="mt-4 p-title">Perkenalkan Saya Muhammad Fakhri Mubarok Wasis Putra, Mahasiswa lulusan Universitas
                Pembangunan
                Nasional "Veteran" Jakarta dan berikut hasil project test saya.</p>
        </div>
    </div>

    <footer id="footer">

        <!-- Bootstrap core JavaScript-->

    </footer><!-- End Footer -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- JS Files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>



</body>

</html>