<?= $this->extend('auth/layout'); ?>

<?= $this->section('content') ?>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <nav class=" navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="col-5">
                <a class=" navbar-brand" href="<?= base_url(); ?>"><strong>CORALIS STUDIO</strong></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="col d-flex justify-content-end">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="<?= base_url(); ?>">Home</a>
                    <a class="nav-item nav-link active" href="<?= base_url(); ?>login">Login</a>
                    <a class="nav-item nav-link" href="<?= base_url(); ?>register">Register</a>
                </div>
            </div>
        </div>
    </nav>
</header>
<!-- End Header -->

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center pt-5">

        <div class="col-xl-4 col-lg-4 col-md-2 pt-5 pb-4">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <?php if (isset($validation)): ?>
                                    <div class="col-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= $validation->listErrors() ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (session()->getFlashdata('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->getFlashdata('success') ?>
                                    </div>
                                <?php endif; ?>
                                <form class="user" action="<?= base_url('login') ?>" method="post">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" placeholder="Enter email" name="email">
                                    </div>
                                    <div class="form-group mt-2">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="password" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" placeholder="Enter password" name="password">
                                        <a class="smaller" href="<?= base_url(); ?>forgotpassword"
                                            style="text-decoration:none;font-size:0.78rem">Forgot Password?</a>
                                    </div>
                                    <div class="form-group d-flex justify-content-center mt-3">
                                        <button type="submit" href="index.html"
                                            class="btn btn-primary btn-user btn-block form-control">
                                            Login
                                        </button>
                                    </div>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url(); ?>/register"
                                        style="text-decoration:none">Create an Account!</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?= $this->endSection() ?>