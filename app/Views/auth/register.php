<?= $this->extend('auth/layout'); ?>

<?= $this->section('content') ?>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <nav class=" navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="col-5">
                <a class=" navbar-brand" href="<?= base_url(); ?>"><strong>CORALIS STUDIO</strong></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="col d-flex justify-content-end">
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="<?= base_url(); ?>">Home</a>
                    <a class="nav-item nav-link" href="<?= base_url(); ?>login">Login</a>
                    <a class="nav-item nav-link active" href="<?= base_url(); ?>register">Register</a>
                </div>
            </div>
        </div>
    </nav>
</header>
<!-- End Header -->

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center pt-5">

        <div class="col-xl-5 col-lg-5 col-md-3 pt-5 pb-4">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-3">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h5 mb-4 ">Silakan Mendaftar Akun Baru</h1>
                                </div>
                                <?php if (session()->getFlashdata('error')): ?>
                                    <div class="col-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= session()->getFlashdata('error'); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (session()->getFlashdata('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->getFlashdata('success') ?>
                                    </div>
                                <?php endif; ?>
                                <form class="user" action="<?= base_url('register') ?>" method="post"
                                    enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" placeholder="Enter email" name="email">
                                    </div>
                                    <div class="form-group  mt-1">
                                        <label for="exampleInputEmail1">Fullname</label>
                                        <input type="text" class="form-control" placeholder="Enter fullname"
                                            name="fullname">
                                    </div>
                                    <div class="form-group  mt-3">
                                        <label for="exampleInputEmail1" class="mb-3">Photo Profile</label>
                                        <div id="alertMessage" class="alert alert-warning mb-3" style="display: none">
                                            <span id="alertMsg"></span>
                                        </div>
                                        <div class="d-grid text-center justify-content-center d-flex">
                                            <img class="mb-3" id="ajaxImgUpload" alt="Preview Image"
                                                src="https://via.placeholder.com/300"
                                                style=" border-radius: 50%; width:10rem;height:10rem" />
                                        </div>
                                        <div class="mb-3">
                                            <input type="file" name="img" multiple="true" id="finput"
                                                onchange="onFileUpload(this);" class="form-control" accept="image/*">
                                        </div>
                                    </div>
                                    <div class="form-group mt-1">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="password" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" placeholder="Enter password" name="password">
                                    </div>
                                    <div class="form-group mt-1">
                                        <label for="exampleInputEmail1">Confirm Password</label>
                                        <input type="password" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" placeholder="Re-enter password"
                                            name="password_confirm">
                                    </div>
                                    <div class="form-group d-flex justify-content-center mt-4">
                                        <button type="submit" href="index.html"
                                            class="btn btn-primary btn-user btn-block form-control">
                                            Register
                                        </button>
                                    </div>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?= base_url(); ?>/login" style="text-decoration:none">Have
                                        an account? Login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function onFileUpload(input, id) {
            id = id || '#ajaxImgUpload';
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(id).attr('src', e.target.result)
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function () {
            $('#upload_image_form').on('submit', function (e) {
                $('.uploadBtn').html('Uploading ...');
                $('.uploadBtn').prop('Disabled');
                e.preventDefault();
                if ($('#file').val() == '') {
                    alert("Choose File");
                    $('.uploadBtn').html('Upload');
                    $('.uploadBtn').prop('enabled');
                    document.getElementById("upload_image_form").reset();
                } else {
                    $.ajax({
                        url: "<?php echo base_url(); ?>/AjaxFileUpload/upload",
                        method: "POST",
                        data: new FormData(this),
                        processData: false,
                        contentType: false,
                        cache: false,
                        dataType: "json",
                        success: function (res) {
                            console.log(res.success);
                            if (res.success == true) {
                                $('#ajaxImgUpload').attr('src', 'https://via.placeholder.com/300');
                                $('#alertMsg').html(res.msg);
                                $('#alertMessage').show();
                            } else if (res.success == false) {
                                $('#alertMsg').html(res.msg);
                                $('#alertMessage').show();
                            }
                            setTimeout(function () {
                                $('#alertMsg').html('');
                                $('#alertMessage').hide();
                            }, 4000);
                            $('.uploadBtn').html('Upload');
                            $('.uploadBtn').prop('Enabled');
                            document.getElementById("upload_image_form").reset();
                        }
                    });
                }
            });
        });
    </script>
    <?= $this->endSection() ?>