<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Authentication App (Fakhri)</title>

    <!-- Custom fonts for this template-->
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">


    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">

</head>

<body class="bg-primary">

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top ">
        <nav class=" navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="col-5">
                    <a class=" navbar-brand" href="#"><strong>CORALIS STUDIO</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </nav>
    </header>
    <!-- End Header -->

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center pt-5">

            <div class="col-xl-5 col-lg-5 col-md-3 pt-5 pb-4">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-3">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h5 mb-4 ">Profil Akun</h1>
                                    </div>
                                    <div class="form-group ">
                                        <div class="d-grid text-center justify-content-center d-flex">
                                            <img class="mb-1" id="ajaxImgUpload" alt="Preview Image"
                                                src="<?= base_url()?>uploads/<?= session()->get('img');?>"
                                                style=" border-radius: 50%; width:10rem;height:10rem" />
                                        </div>
                                    </div>
                                    <?php if (session()->getFlashdata('error')): ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= session()->getFlashdata('error') ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (session()->getFlashdata('success')): ?>
                                    <div class="alert alert-success" role="alert">
                                        <?= session()->getFlashdata('success') ?>
                                    </div>
                                <?php endif; ?>
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1"
                                            aria-describedby="emailHelp" value="  <?= session()->get('email') ?>"
                                            name="email" disabled>
                                    </div>
                                    <div class="form-group  mt-1">
                                        <label for="exampleInputEmail1">Fullname</label>
                                        <input type="text" class="form-control"  value="  <?= session()->get('fullname') ?>"
                                            name="fullname" disabled>
                                    </div>
                                    <div class="form-group d-flex justify-content-center mt-4">
                                    <a  href="<?= base_url('loggedin/changepassword/')?><?= session()->get('token') ?>"
                                            class="btn btn-primary btn-user btn-block form-control">
                                            Change Password
                                        </a>
                                </div>
                                <div class="form-group d-flex justify-content-center mt-3">

                                        <a  href="<?= base_url('logout')?>"
                                            class="btn btn-primary btn-user btn-block form-control">
                                            Logout
                                        </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    </div>
    <footer id="footer">

        <!-- Bootstrap core JavaScript-->

    </footer><!-- End Footer -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- JS Files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>



</body>

</html>