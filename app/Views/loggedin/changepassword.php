<?= $this->extend('auth/layout'); ?>

<?= $this->section('content') ?>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
    <nav class=" navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="col-5">
                <a class=" navbar-brand" href="#"><strong>CORALIS STUDIO</strong></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
</header>
<!-- End Header -->

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center pt-5">

        <div class="col-xl-5 col-lg-5 col-md-3 pt-5 pb-4">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="form-group">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Change Password</h1>
                                    </div>
                                    <?php if (session()->getFlashdata('error')): ?>
                                        <div class="col-12">
                                            <div class="alert alert-danger" role="alert">
                                                <?= session()->getFlashdata('error'); ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?= form_open(); ?>
                                    <label for="exampleInputEmail1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter password" name="password">
                                </div>
                                <div class="form-group mt-2">
                                    <label for="exampleInputEmail1">Confirm Password</label>
                                    <input type="password" class="form-control" id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Enter password"
                                        name="confirm_password">
                                </div>
                                <div class="form-group d-flex justify-content-center mt-3">
                                    <input type="submit" class="btn btn-primary btn-user btn-block form-control">
                                    </input>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <?= $this->endSection() ?>