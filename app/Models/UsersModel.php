<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $DBGroup = 'default';
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = false;
    protected $protectFields = true;
    protected $allowedFields = ['email', 'password', 'fullname', 'img', 'token'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat = 'datetime';
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    // Validation
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert = [];
    protected $afterInsert = [];
    protected $beforeUpdate = [];
    protected $afterUpdate = [];
    protected $beforeFind = [];
    protected $afterFind = [];
    protected $beforeDelete = [];
    protected $afterDelete = [];


    public function updatedAt($token)
    {
        date_default_timezone_set("Asia/Jakarta");
        $sql = 'UPDATE users SET updated_at = "' . date('Y-m-d H:i:s') . '" where token = "' . $token . '";';
        $query = $this->db->query($sql);


        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function verifyToken($token)
    {
        $builder = $this->db->table('users');
        $builder->select('token,fullname,updated_at');
        $builder->where('token', $token);
        $result = $builder->get();
        if (count($result->getResultArray()) == 1) {
            return $result->getRowArray();
        } else {
            return false;
        }
    }

    public function checkExpiryDate($time)
    {
        date_default_timezone_set("Asia/Jakarta");
        $timediff = (strtotime(date('Y-m-d H:i:s')) - strtotime($time)) / 60;

        if ($timediff < 900) {
            return true;
        } else {
            return false;
        }
    }

    public function updatePass($token, $password)
    {
        $builder = $this->db->table('users');
        $builder->select('token,fullname,updated_at');
        $builder->where('token', $token);
        $builder->update(['password' => $password]);


        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
}