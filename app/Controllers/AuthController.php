<?php

namespace App\Controllers;

use App\Models\UsersModel;
use CodeIgniter\I18n\Time;

class AuthController extends BaseController
{
    public function viewLogin()
    {
        return view('auth/login');
    }
    public function login()
    {
        $users = new UsersModel();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        $dataUser = $users->where([
            'email' => $email,
        ])->first();
        if ($dataUser) {
            if (password_verify($password, $dataUser['password'])) {
                session()->set([
                    'email' => $dataUser['email'],
                    'fullname' => $dataUser['fullname'],
                    'img' => $dataUser['img'],
                    'token' => $dataUser['token'],
                    'isLoggedIn' => TRUE
                ]);
                return redirect()->to(base_url('loggedin'));
            } else {
                session()->setFlashdata('error', 'E-mail & Password Salah');
                return redirect()->back();
            }
        } else {
            session()->setFlashdata('error', 'E-mail & Password Salah');
            return redirect()->back();
        }
    }


    public function viewRegister()
    {
        return view('auth/register');
    }
    public function register()
    {
        helper(['form', 'url']);


        if ($this->request->getMethod() == 'post') {
            //let's do the validation here


            if (
                !$this->validate([
                    'fullname' => [
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'Nama Lengkap Harus diisi'
                        ]
                    ],
                    'email' => [
                        'rules' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
                        'errors' => [
                            'required' => 'Email Harus diisi',
                            'valid_email' => 'Format Email Harus Valid'

                        ]
                    ],
                    'img' => [
                        'rules' => 'uploaded[img]|mime_in[img,image/png,image/jpeg],image/jpg|max_size[img,2048]',
                        'errors' => [
                            'uploaded' => 'Harus Ada File yang diupload',
                            'mime_in' => 'File Extention Harus Berupa jpg, jpeg atau png',
                            'max_size' => 'Ukuran File Maksimal 2 MB'
                        ]
                    ],
                    'password' => [
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'Password Harus diisi'
                        ]
                    ],
                    'password_confirm' => [
                        'rules' => 'matches[password]',
                        'errors' => [
                            'required' => '{field} tidak sama'
                        ]
                    ],
                ])
            ) {
                session()->setFlashdata('error', $this->validator->listErrors());
                return redirect()->back()->withInput();
            } else {
                $model = new UsersModel();

                $dataImg = $this->request->getFile('img');

                $fileImg = $dataImg->getName();
                $dataImg->move('uploads/', $fileImg);

                $generateToken = md5(str_shuffle('abcdefghijklmnopqrstuvwxyz' . time()));
                $newData = [
                    'email' => $this->request->getVar('email'),
                    'img' => $fileImg,
                    'token' => $generateToken,
                    'fullname' => $this->request->getVar('fullname'),
                    'password' => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
                ];
                $model->save($newData);
                $session = session();
                $session->setFlashdata('success', 'Successful Registration');
                return redirect()->to(base_url('login'));
            }
        }
        return view('auth/register');
    }

    public function index()
    {
        return view('home');
    }



    public function forgotPassword()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() == 'post') {
            if (
                !$this->validate([
                    'email' => [
                        'rules' => 'required|valid_email',
                        'errors' => [
                            'required' => 'Email Harus diisi',
                            'valid_email' => 'Format Email Harus Valid'

                        ]
                    ],
                ])
            ) {

                session()->setFlashdata('error', $this->validator->listErrors());
                return redirect()->back()->withInput();
            } else {
                $model = new UsersModel();
                $email = $this->request->getVar('email');
                $userData = $model->where('email', $email)->first();
                if (!empty($userData)) {
                    if ($model->updatedAt($userData['token'])) {
                        $to = $email;
                        $subject = 'Reset Password Link - Auth App Fakhri';
                        $token = $userData['token'];
                        $message = 'Hi ' . $userData['fullname'] . '<br><br>' . 'Untuk mereset password anda klik link berikut' . '<br>'
                            . '<a href="' . base_url() . 'resetpassword/' . $token . '">Click Disini</a> <br><br>' . 'Terimakasih!<br>Auth App Fakhri ';
                        $myemail = \Config\Services::email();
                        $myemail->setTo($to);
                        $myemail->setFrom('info@authappfakhri.in', 'AuthAppFakhri');
                        $myemail->setSubject($subject);
                        $myemail->setMessage($message);
                        if ($myemail->send()) {
                            session()->setFlashdata('success', 'Reset password link have been sent to your email address');
                            return redirect()->back()->withInput();
                        } else {
                            session()->setFlashdata('error', $myemail->printDebugger(['headers']));
                            return redirect()->back()->withInput();
                        }
                    } else {
                        session()->setFlashdata('error', 'Sorry unable to update, try again later.');
                        return redirect()->back()->withInput();
                    }
                } else {
                    session()->setFlashdata('error', 'Email Does Not Exist');
                    return redirect()->back()->withInput();
                }

            }
        }
        return view('auth/forgotpassword');
    }

    public function resetPassword($token = null)
    {
        helper(['form', 'url']);
        $user = new UsersModel();
        $datauser = $user->where(['token' => $token])->first();
        $data = [
            'user_data' => $user->where(['token' => $token])->first(),
        ];
        if (!empty($token)) {
            $userdata = $user->verifyToken($token);
            if (!empty($userdata)) {
                if ($user->checkExpiryDate($datauser['updated_at'])) {
                    if ($this->request->getMethod() == 'post') {
                        if (
                            !$this->validate([
                                'password' => [
                                    'rules' => 'required',
                                    'errors' => [
                                        'required' => 'Password Harus diisi'
                                    ]
                                ],
                                'confirm_password' => [
                                    'rules' => 'required|matches[password]',
                                    'errors' => [
                                        'required' => 'Confirmasi password harus diisi'
                                    ]
                                ],
                            ])
                        ) {
                            session()->setFlashdata('error', $this->validator->listErrors());
                            return redirect()->back()->withInput();
                        } else {
                            $password = password_hash($this->request->getVar('password'), PASSWORD_BCRYPT);
                            if ($user->updatePass($token, $password)) {
                                $session = session();
                                $session->setFlashdata('success', 'Password has been changed');
                                return redirect()->to(base_url('login'));
                            } else {
                                $session = session();
                                $session->setFlashdata('error', 'Sorry unable to update. Try again later!');
                                return redirect()->to(base_url('login'));
                            }
                        }
                    }
                } else {
                    $data['error'] = 'Reset password link expired';
                }
            } else {
                $data['error'] = 'Unable to find user account';
            }
        } else {
            $data['error'] = 'Sorry Unaouthorized Access';
        }

        return view('auth/resetpassword', $data);
    }

    public function home()
    {

        $data = [];
        $model = new UsersModel();

        $data['users'] = $model->where('email', session()->get('email'))->first();
        return view('loggedin/index', $data);
    }

    public function changePassword($token)
    {
        helper(['form', 'url']);
        $user = new UsersModel();

        if ($this->request->getMethod() == 'post') {
            if (
                !$this->validate([
                    'password' => [
                        'rules' => 'required',
                        'errors' => [
                            'required' => 'Password Harus diisi'
                        ]
                    ],
                    'confirm_password' => [
                        'rules' => 'required|matches[password]',
                        'errors' => [
                            'required' => 'Confirmasi password harus diisi'
                        ]
                    ],
                ])
            ) {
                session()->setFlashdata('error', $this->validator->listErrors());
                return redirect()->back()->withInput();
            } else {
                $password = password_hash($this->request->getVar('password'), PASSWORD_BCRYPT);
                if ($user->updatePass($token, $password)) {
                    $session = session();
                    $session->setFlashdata('success', 'Password has been changed');
                    return redirect()->to(base_url('loggedin'));
                } else {
                    $session = session();
                    $session->setFlashdata('error', 'Sorry unable to update. Try again later!');
                    return redirect()->to(base_url('loggedin'));
                }
            }
        }
        return view('loggedin/changepassword');
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to('login');
    }

}